#!/bin/sh

set -e

FILES="""
googleapis-master/google/api/annotations.proto
googleapis-master/google/api/http.proto
"""
# """
# googleapis-master/google/api/annotations.proto
# googleapis-master/google/api/http.proto
# googleapis-master/google/rpc/code.proto
# googleapis-master/google/rpc/error_details.proto
# googleapis-master/google/rpc/status.proto
# """

curl -sSL https://github.com/googleapis/googleapis/archive/master.tar.gz | tar --strip 1 -xz $FILES


