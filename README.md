# PRotoREPO

*On commit*:

1. Gather remote dependencies using `sh ./bin/dependencies.sh`
2. If there are differences, commit and push back to itself
3. Build with prototool, output to submodules
4. Commit and push to all submodules
